<?php

namespace Drupal\Tests\testing_example\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test the TskService.
 *
 * @group tsk
 *
 * @ingroup tsk
 */
class TskServiceTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   *
   * Any modules added here will be loaded, along with anything in $modules in
   * parent classes.
   *
   * These modules are not installed, but their services and hooks are
   * available.
   *
   * @var string[]
   */
  protected static $modules = ['tsk'];

  /**
   * Test the TskService.
   */
  public function testTskService() {

    // Create a private tempstore.
    /** @var \Drupal\Core\TempStore\PrivateTempStore $store_private */
    $store_private = \Drupal::service('tempstore.private')->get('tsk');
    $store_private->set('test1', 'This tempstore will be deleted.');
    $store_private->set('test2', 'This tempstore will not be deleted.');

    // Create a shared tempstore.
    /** @var \Drupal\Core\TempStore\SharedTempStore $store_shared */
    $store_shared = \Drupal::service('tempstore.shared')->get('tsk');
    $store_shared->set('test3', 'This tempstore will be deleted.');
    $store_shared->set('test4', 'This tempstore will not be deleted.');

    // Test if the temporary storages are created.
    $this->assertEquals('This tempstore will be deleted.', $store_private->get('test1'));
    $this->assertEquals('This tempstore will not be deleted.', $store_private->get('test2'));
    $this->assertEquals('This tempstore will be deleted.', $store_shared->get('test3'));
    $this->assertEquals('This tempstore will not be deleted.', $store_shared->get('test4'));

    // Kill the private tempstore.
    /** @var \Drupal\tsk\Services\TskService $tsk_service */
    $tsk_service = \Drupal::service('tsk.service');
    $tsk_service->kill('tsk', 'private', FALSE, 'test1');

    // Test if the private tempstore is deleted.
    $this->assertNull($store_private->get('test1'));

    // Test if the private tempstore is not deleted.
    $this->assertEquals('This tempstore will not be deleted.', $store_private->get('test2'));

    // Kill the shared tempstore.
    $tsk_service->kill('tsk', 'shared', FALSE, 'test3');

    // Test if the shared tempstore is deleted.
    $this->assertNull($store_shared->get('test3'));

    // Test if the shared tempstore is not deleted.
    $this->assertEquals('This tempstore will not be deleted.', $store_shared->get('test4'));

    // Kill all the private temporary storages.
    $tsk_service->kill('tsk', 'private');

    // Test if the private temporary storages are deleted.
    $this->assertNull($store_private->get('test2'));

    // Kill all the shared temporary storages.
    $tsk_service->kill('tsk', 'shared');

    // Test if the shared temporary storages are deleted.
    $this->assertNull($store_shared->get('test4'));
  }

}
