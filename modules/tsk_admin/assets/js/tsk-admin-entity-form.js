(function ($, Drupal) {
  Drupal.behaviors.entityForm = {
    attach(context, settings) {
      $('caption').click(function () {
        $('tbody').toggle();
        $('thead').toggle();
      });
    },
  };
})(jQuery, Drupal);
