<?php

namespace Drupal\tsk_admin\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\tsk_admin\Entity\TskEntity;
use Drupal\tsk_admin\Services\TskAdminService;
use Drupal\tsk\Services\TskService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines TskController class.
 */
class TskAdminController extends ControllerBase {

  /**
   * Defines tskAdminService.
   *
   * @var \Drupal\tsk_admin\Services\TskAdminService
   */
  protected $tskAdminService;

  /**
   * Defines tskService.
   *
   * @var \Drupal\tsk\Services\TskService
   */
  protected $tskService;

  /**
   * TskController constructor.
   *
   * @param \Drupal\tsk_admin\Services\tskAdminService $tsk_admin_service
   *   Temporary Storage Killer admin service.
   * @param \Drupal\tsk\Services\tskService $tsk_service
   *   Temporary Storage Killer service.
   */
  public function __construct(tskAdminService $tsk_admin_service, tskService $tsk_service) {
    $this->tskAdminService = $tsk_admin_service;
    $this->tskService = $tsk_service;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tsk_admin.service'),
      $container->get('tsk.service')
    );
  }

  /**
   * Delete temporary storage for the specified TSK entity.
   *
   * @param \Drupal\tsk\Entity\TskEntity $tsk_entity
   *   Temporary storage entity.
   */
  public function kill(TskEntity $tsk_entity) {
    $this->tskService->kill($tsk_entity->collection, $tsk_entity->type, $tsk_entity->kill_all, $tsk_entity->key);
    $this->messenger()->addStatus($this->t('The temporary storage has been deleted.'));
    return $this->redirect('entity.tsk_entity');
  }

  /**
   * Delete all specified temporary storages.
   */
  public function killAll() {
    $this->tskAdminService->killAll();
    $this->messenger()->addStatus($this->t('The temporary storages have been deleted.'));
    return $this->redirect('entity.tsk_entity');
  }

}
