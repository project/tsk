<?php

namespace Drupal\tsk_admin;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a list of TSK entities.
 */
class TskEntityListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    // Prepare the table header.
    $header = [];
    $header['id'] = $this->t('Id');
    $header['type'] = $this->t('Tempstore type');
    $header['collection'] = $this->t('Collection');
    $header['kill_all'] = $this->t('Kill all');
    $header['key'] = $this->t('Key');

    // Return the table header.
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    // Prepare the table row for the directory.
    $row = [];
    $row['id'] = $entity->id();
    $row['type'] = $entity->type;
    $row['collection'] = $entity->collection;
    $row['kill_all'] = $entity->kill_all;
    $row['key'] = $entity->key;

    // Return the table row.
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    // Prepare the table row operations for the directory.
    $operations = parent::getDefaultOperations($entity);
    if ($entity->hasLinkTemplate('edit')) {
      $operations['edit'] = [
        'title' => $this->t('Edit entity'),
        'weight' => 20,
        'url' => $entity->toUrl('edit'),
      ];
    }
    if ($entity->hasLinkTemplate('delete')) {
      $operations['delete'] = [
        'title' => $this->t('Delete entity'),
        'weight' => 40,
        'url' => $entity->toUrl('delete'),
      ];
    }
    if ($entity->hasLinkTemplate('kill')) {
      $operations['kill'] = [
        'title' => $this->t('Kill the temporary storage'),
        'weight' => 40,
        'url' => $entity->toUrl('kill'),
      ];
    }

    // Return the table row operations.
    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    $build['table']['#empty'] = $this->t('Here can you define, which temporary storages will be deleted.');

    $url = Url::fromRoute('entity.tsk_entity.kill_all');
    $url_options = [
      'attributes' => [
        'class' => [
          'button',
          'button-action',
        ],
      ],
    ];
    $url->setOptions($url_options);

    $build['kill_all_link'] = [
      '#type' => 'link',
      '#url' => $url,
      '#title' => $this->t('Kill all'),
    ];

    return $build;
  }

}
