<?php

namespace Drupal\tsk_admin\Commands;

use Drush\Commands\DrushCommands;
use Drupal\tsk_admin\Services\TskAdminServiceInterface;

/**
 * Drush integration for the module Temporary Storages Killer.
 */
class TskAdminCommands extends DrushCommands {

  /**
   * TskAdminService.
   *
   * @var \Drupal\tsk\Services\TskAdminServiceInterface
   */
  protected $tskAdminService;

  /**
   * TskAdminCommands constructor.
   *
   * @param \Drupal\tsk_admin\Services\TskAdminServiceInterface $tsk_admin_service
   *   Temporary Storage Killer Admin service.
   */
  public function __construct(TskAdminServiceInterface $tsk_admin_service) {
    $this->tskAdminService = $tsk_admin_service;
  }

  /**
   * Kills the specified temporary storages.
   *
   * @command temp-store-killer:all
   * @aliases tska
   * @usage drush temp-store-killer:all
   *   Kills the specified temporary storages.
   */
  public function killAll() {
    $this->tskAdminService->killAll();
  }

}
