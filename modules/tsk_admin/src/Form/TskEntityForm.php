<?php

namespace Drupal\tsk_admin\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * TSK entity form for the Temporary Storage Killer.
 */
class TskEntityForm extends EntityForm {

  /**
   * The database connection to be used.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a new TskEntityForm.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to be used.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $tsk_entity = $this->entity;

    // Prepare the form.
    $form = parent::form($form, $form_state);

    // Attach the custom library.
    $form['#attached']['library'][] = 'tsk_admin/entityForm';

    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Tempstore type'),
      '#default_value' => $tsk_entity->type,
      '#description' => $this->t('Type of the tempstore.'),
      '#options' => [
        'private' => $this->t('private'),
        'shared' => $this->t('shared'),
      ],
    ];

    $form['collection'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Collection'),
      '#size' => 60,
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => $tsk_entity->collection,
      '#description' => $this->t('The collection name to use for the key/value store. This is typically a shared namespace or module name, e.g. views, entity, etc.'),
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#disabled' => !$tsk_entity->isNew(),
      '#default_value' => $tsk_entity->id(),
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'source' => ['collection'],
      ],
    ];

    $form['kill_all'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Kill all'),
      '#default_value' => $tsk_entity->kill_all,
      '#description' => $this->t('Deletes all stored key/value pairs from the collection.'),
    ];
    $form['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key'),
      '#default_value' => $tsk_entity->key,
      '#description' => $this->t('The key of the stored data.'),
      '#states' => [
        // Only show this field when the 'kill_all' checkbox is disabled.
        'visible' => [
          ':input[name="kill_all"]' => [
            'checked' => FALSE,
          ],
        ],
      ],
    ];

    $query = $this->database->select('key_value_expire', 'kve');
    $query->fields('kve', ['collection', 'name']);
    $query->condition('kve.collection', $query->escapeLike('tempstore.') . '%', 'LIKE');
    $result = $query->execute()->fetchAll();

    $form['storages'] = [
      '#type' => 'table',
      '#caption' => $this->t('Click to display (hide) temporary storages, which are recently in the database.'),
      '#header' => [$this->t('Type'), $this->t('Collection'), $this->t('Key')],
      '#sticky' => TRUE,
      '#empty' => $this->t('There are no temporary storages in the database'),
    ];
    $remove_from_collection = ['tempstore.private.', 'tempstore.shared.'];
    foreach ($result as $i => $row) {
      $type = str_contains($row->collection, '.private.') ? $this->t('private') : $this->t('shared');
      $form['storages'][$i]['type'] = [
        '#type' => 'html_tag',
        '#tag' => 'pre',
        '#value' => $type,
      ];
      $form['storages'][$i]['collection'] = [
        '#type' => 'html_tag',
        '#tag' => 'pre',
        '#value' => str_replace($remove_from_collection, '', $row->collection),
      ];
      $form['storages'][$i]['key'] = [
        '#type' => 'html_tag',
        '#tag' => 'pre',
        '#value' => $row->name,
      ];
    }

    // Return the form.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $value = parent::save($form, $form_state);

    $this->messenger()->addStatus($this->t('The entity %id was saved successfully.', ['%id' => $this->entity->id]));

    // Redirect to the proper URL.
    $form_state->setRedirect('entity.tsk_entity');

    return $value;
  }

  /**
   * Checks if the TSK entity exists.
   *
   * @param string $id
   *   Machine name (ID) of the TSK entity.
   *
   * @return bool
   *   Returns true, if the TSK entity exist.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function exists($id) {
    $storage = $this->entityTypeManager->getStorage('tsk_entity');
    $tsk_entity = $storage->getQuery()->accessCheck(FALSE)->condition('id', $id)->execute();
    return (bool) $tsk_entity;
  }

}
