<?php

namespace Drupal\tsk_admin\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Delete form for the TSK entity.
 */
class TskEntityDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $tsk_entity = $this->entity;
    return $this->t('Are you sure you want to delete the entity %id from the control list?', ['%id' => $tsk_entity->id]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.tsk_entity');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $tsk_entity = $this->entity;
    // Delete the TSK entity and display the status message.
    try {
      $tsk_entity->delete();

      $this->messenger()->addStatus($this->t('The entity %id was deleted successfully.', ['%id' => $tsk_entity->id]));
    }
    catch (EntityStorageException $exception) {
      $this->messenger()->addError($this->t('The entity %id was not deleted.', ['%id' => $tsk_entity->id]));
    }
    // Set form redirection.
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
