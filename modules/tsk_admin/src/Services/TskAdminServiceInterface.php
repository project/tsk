<?php

namespace Drupal\tsk_admin\Services;

/**
 * Provides methods to delete temporary storages.
 *
 * @package Drupal\tsk
 */
interface TskAdminServiceInterface {

  /**
   * Deletes all temporary storages specified as TSK config entity.
   */
  public function killAll(): void;

}
