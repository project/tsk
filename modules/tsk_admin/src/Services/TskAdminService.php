<?php

namespace Drupal\tsk_admin\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\tsk\Services\TskServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides methods to delete temporary storages.
 *
 * @package Drupal\tsk
 */
class TskAdminService implements TskAdminServiceInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Temporary Storage Killer service.
   *
   * @var \Drupal\tsk\Services\TskServiceInterface
   */
  protected $tskService;

  /**
   * Constructs a TskService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\tsk\Services\TskServiceInterface $tsk_service
   *   Temporary Storage Killer service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TskServiceInterface $tsk_service) {
    $this->entityTypeManager = $entity_type_manager;
    $this->tskService = $tsk_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('tsk.service')
    );
  }

  /**
   * Deletes all temporary storages specified as TSK config entity.
   */
  public function killAll(): void {
    $tsk_entities = $this->entityTypeManager->getStorage('tsk_entity')->loadMultiple();
    foreach ($tsk_entities as $tsk_entity) {
      $this->tskService->kill($tsk_entity->collection, $tsk_entity->type, $tsk_entity->kill_all, $tsk_entity->key);
    }
  }

}
