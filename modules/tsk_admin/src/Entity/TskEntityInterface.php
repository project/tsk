<?php

namespace Drupal\tsk_admin\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface for Temporary Storage Killer entities.
 */
interface TskEntityInterface extends ConfigEntityInterface {
}
