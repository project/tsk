<?php

namespace Drupal\tsk_admin\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Temporary Storage Killer entity.
 *
 * @ConfigEntityType(
 *   id = "tsk_entity",
 *   label = @Translation("Temporary Storage Killer Entity"),
 *   module = "tsk_admin",
 *   config_prefix = "tsk",
 *   handlers = {
 *     "list_builder" = "Drupal\tsk_admin\TskEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\tsk_admin\Form\TskEntityForm",
 *       "edit" = "Drupal\tsk_admin\Form\TskEntityForm",
 *       "delete" = "Drupal\tsk_admin\Form\TskEntityDeleteForm",
 *     }
 *   },
 *   admin_permission = "administer temporary storage killer",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *   config_export = {
 *     "id",
 *     "type",
 *     "collection",
 *     "kill_all",
 *     "key"
 *   },
 *   links = {
 *     "edit" = "/admin/config/development/performance/tsk/{tsk_entity}",
 *     "delete" = "/admin/config/development/performance/tsk/{tsk_entity}/delete",
 *     "kill" = "/admin/config/development/performance/tsk/{tsk_entity}/kill",
 *   },
 * )
 */
class TskEntity extends ConfigEntityBase implements TskEntityInterface {

  /**
   * ID of the Temp Store Killer Entity.
   *
   * @var string
   */
  public $id = NULL;

  /**
   * Type of the collection: private / shared.
   *
   * @var string
   */
  public $type = 'private';

  /**
   * The collection name to use for this key/value store.
   *
   * @var string
   */
  public $collection = NULL;

  /**
   * Kill all (true) or a single item (false)?
   *
   * @var bool
   */
  public $kill_all = TRUE;

  /**
   * The key of the stored data.
   *
   * @var string
   */
  public $key = NULL;

}
