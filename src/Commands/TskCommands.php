<?php

namespace Drupal\tsk\Commands;

use Drush\Commands\DrushCommands;
use Drupal\tsk\Services\TskServiceInterface;

/**
 * Drush integration for the module Temporary Storages Killer.
 */
class TskCommands extends DrushCommands {

  /**
   * TskService.
   *
   * @var \Drupal\tsk\Services\TskServiceInterface
   */
  protected $tskService;

  /**
   * TskCommands constructor.
   *
   * @param \Drupal\tsk\Services\TskServiceInterface $tsk_service
   *   Temporary Storage Killer service.
   */
  public function __construct(TskServiceInterface $tsk_service) {
    $this->tskService = $tsk_service;
  }

  /**
   * Kills a temporary storage.
   *
   * @param string $type
   *   Type of the collection: private / shared.
   * @param string $collection
   *   The collection name to use for this key/value store.
   * @param string $key
   *   The key of the stored data.
   *
   * @command temp-store-killer
   * @aliases tsk
   *
   * @usage drush temp-store-killer type collection key
   *   Kills a temporary storage.
   */
  public function kill(string $type, string $collection, string $key = '') {
    $kill_all = empty($key);
    $this->tskService->kill($collection, $type, $kill_all, $key);
  }

}
