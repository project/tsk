<?php

namespace Drupal\tsk\Services;

use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\TempStore\SharedTempStoreFactory;

/**
 * Provides methods to delete temporary storages.
 *
 * @package Drupal\tsk
 */
class TskService implements TskServiceInterface {

  /**
   * The storage factory creating the backend to store the data.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface
   */
  protected $storageFactory;

  /**
   * The private tempstore factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $privateTempStoreFactory;

  /**
   * The shared tempstore factory.
   *
   * @var \Drupal\Core\TempStore\SharedTempStoreFactory
   */
  protected $sharedTempStoreFactory;

  /**
   * Constructs a TskService object.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface $storage_factory
   *   The key/value store factory.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $private_tempstore_factory
   *   The private tempstore factory.
   * @param \Drupal\Core\TempStore\SharedTempStoreFactory $shared_tempstore_factory
   *   The shared tempstore factory.
   */
  public function __construct(KeyValueExpirableFactoryInterface $storage_factory, PrivateTempStoreFactory $private_tempstore_factory, SharedTempStoreFactory $shared_tempstore_factory) {
    $this->storageFactory = $storage_factory;
    $this->privateTempStoreFactory = $private_tempstore_factory;
    $this->sharedTempStoreFactory = $shared_tempstore_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function kill(string $collection, string $type = 'private', bool $kill_all = TRUE, string $key = ''): void {

    if ($kill_all) {
      $storage = $this->storageFactory->get("tempstore.$type.$collection");
      $storage->deleteAll();
    }
    elseif ($type === 'private') {
      $storage = $this->privateTempStoreFactory->get($collection);
      $storage->delete($key);
    }
    elseif ($type === 'shared') {
      $storage = $this->sharedTempStoreFactory->get($collection);
      $storage->delete($key);
    }
  }

}
