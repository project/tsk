<?php

namespace Drupal\tsk\Services;

/**
 * Provides methods to delete temporary storages.
 *
 * @package Drupal\tsk
 */
interface TskServiceInterface {

  /**
   * Deletes a selected temporary storage.
   *
   * @param string $collection
   *   The collection name to use for this key/value store.
   * @param string $type
   *   Tempstore type. E.g. private or shared.
   * @param bool $kill_all
   *   Kill all (true) or a single item (false)?
   * @param string $key
   *   The key of the stored data.
   */
  public function kill(string $collection, string $type = 'private', bool $kill_all = TRUE, string $key = ''): void;

}
