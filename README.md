CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

The module enables to delete specified temporary storages. There is also the
tsk_admin module, which enables to specify the temporary storages, which should
be killed (if the module tsk_admin is enabled:
admin/config/development/performance/tsk).

The **tsk** module contains a Drush command, that can kill a specified
temporary storage:

    # Delete all private temporary storages in the "email_tfa" collection:
    drush temp-store-killer private email_tfa
    # Delete one single shared temporary storage from the collection
    # "layout_builder.section_storage.overrides" with key "my_page.11.full.en":
    drush tsk shared layout_builder.section_storage.overrides my_page.11.full.en

The **tsk_admin** module contains another Drush command that can be used to kill
all temporary storages specified in the configuration:

    drush temp-store-killer:all
    drush tska

More info:

 * A full description of the module can be found at
   [the project page](https://www.drupal.org/project/tsk).

 * To submit bug reports and feature requests, or to track changes, visit
   [the issue page](https://www.drupal.org/project/issues/tsk).

REQUIREMENTS
------------

There are no required modules.

RECOMMENDED MODULES
-------------------

There are no recommended modules.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See the
   [docs](https://www.drupal.org/documentation/install/modules-themes/modules-8)
   for further information.

CONFIGURATION
-------------

The **tsk** module has no configuration.

The **tsk_admin** module provides the possibility to configure the temporary
storages that should be killed.

You can create a configuration entity for each temporary storage (collection)
in the administration (admin/config/development/performance/tsk). There are the
following fields in the configuration entity form:

 * Tempstore type (you must select "private" or "shared".)
 * Collection (e.g.: "layout_builder.section_storage.overrides", or "email_tfa")
 * Kill all (if checked, all stored key/value pairs from the collection will be
   deleted.)
 * Key (the key of the stored data, e.g.: "my_page.11.full.en")

If you click the button 'Kill all', or execute the Drush command
"drush temp-store-killer:all", all the specified temporary storages will be
deleted.

TROUBLESHOOTING
---------------

There are not any known issues. See:

 * [Issues](https://www.drupal.org/project/issues/tsk)
 * Automated testing (not implemented)

FAQ
---

There are no answers or issues.

MAINTAINERS
-----------

Current maintainers:

 * [Antonín Slejška](https://www.drupal.org/u/anton%C3%ADn-slej%C5%A1ka)
